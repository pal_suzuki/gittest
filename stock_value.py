import pymysql.cursors
import get_sequence

# *********************************************************************
# 在庫金額計算 シミュレーション処理
# 　入庫から出庫を順に取り崩して在庫数と在庫金額をシミュレート
# 　在庫に変動があった日のスナップショットを保存する
# *********************************************************************

def inventory_allocation(cur, rec_ship):

    sql = 'select min(arrival_no) arrival_no, min(arrival_dt) arrival_dt, arrival_price, stock_value from w_stock where item_id = %s and stock_value > 0'
    cur.execute(sql, rec_ship['item_id'])
    result = cur.fetchall()
    rec_stock = result[0]

    #引当残数
    remaining = rec_ship['item_value']
    #在庫残数
    stock_value = 0

    while remaining > 0:
        if not(not rec_stock['arrival_no']):
            # 在庫引当
            if rec_stock['stock_value'] >= remaining:
                print('全数引当可能')
                stock_value = rec_stock['stock_value'] - remaining
                remaining = 0
            else:
                print('一部引当可能')
                remaining = remaining - rec_stock['stock_value']
                stock_value = 0
        else:
            # 引当不能
            print('引当不能')
            # 出荷情報登録
            sql_ins = 'INSERT INTO t_shipment_info(shipment_no, branch_no, shipment_dt, shipment_division, item_id, arrival_no, arrival_price, shipment_value) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)'
            cur.execute(sql_ins, (rec_ship['sa_no'], rec_ship['branch_no'], rec_ship['dt'], rec_ship['division'], rec_ship['item_id'], -1, 0, remaining))

            break  #引当せずにLoop抜ける

        # 在庫更新
        sql_upd = 'update w_stock set stock_value = %s where arrival_no = %s and arrival_dt = %s and item_id = %s'
        cur.execute(sql_upd, (stock_value, rec_stock['arrival_no'], rec_stock['arrival_dt'], rec_ship['item_id']))

        # 出荷情報登録
        print('shipment_no:' + str(rec_ship['sa_no']) + ' branch_no:' + str(rec_ship['branch_no']) + ' arrival_no:' + str(rec_stock['arrival_no']))

        sql_ins = 'INSERT INTO t_shipment_info(shipment_no, branch_no, shipment_dt, shipment_division, item_id, arrival_no, arrival_price, shipment_value) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)'
        cur.execute(sql_ins, (rec_ship['sa_no'], rec_ship['branch_no'], rec_ship['dt'], rec_ship['division'], rec_ship['item_id'], rec_stock['arrival_no'], rec_stock['arrival_price'], (rec_ship['item_value'] - remaining)))

        if remaining != 0:
            cur.execute(sql, rec_ship['item_id'])
            result = cur.fetchall()
            rec_stock = result[0]

    return

################################################################
### メイン処理ここから
################################################################
connection = pymysql.connect(
        host='localhost',
        port=3306,
        user='root',
        password='root',
        db='dashboard_db',
        charset='utf8',
        # cursorclassを指定することでSelect結果をtupleではなくdictionaryで受け取れる
        cursorclass=pymysql.cursors.DictCursor
)

try:
    # 入荷データ
    cur = connection.cursor()
    sql = 'select 1 sa_flag, A.arrival_no sa_no, B.branch_no, A.arrival_dt dt, B.arrival_division division, B.item_id, B.item_value, B.item_price, B.item_amount from t_arrival A inner join t_arrival_detail B on A.arrival_no = B.arrival_no and B.item_price is not null inner join m_item C on B.item_id = C.item_id and B.item_id not in (\'nebiki\', \'pl\', \'syoukai\') where A.arrival_status = 2 and B.arrival_division != 2 union all select 2 sa_flag, A.shipment_no sa_no, B.branch_no, A.shipment_dt dt, A.shipment_division division, B.item_id, B.shipment_value item_value, 0 item_price, 0 item_amount from t_shipment A inner join t_shipment_detail B on A.shipment_no = B.shipment_no and A.shipment_status = 2 and A.shipment_division != 2 inner join m_item C on B.item_id = C.item_id and B.item_id not in (\'nebiki\', \'pl\', \'syoukai\') order by dt, sa_flag, item_id'

    cur.execute(sql)
    result = cur.fetchall()

    loop_end = 0
    count = 0
    item_value = 0
    stock_value = 0

    rec = result[count]
    current_key = rec['dt']
    next_key = ''

    while loop_end == 0:
        try:
            if rec['sa_flag'] == 1:
                # 入荷
                # 在庫ワーク登録
                sql_ins = 'INSERT INTO w_stock(arrival_no, arrival_dt, arrival_division, item_id, arrival_price, stock_value) VALUES (%s, %s, %s, %s, %s, %s)'
                res = cur.execute(sql_ins,(rec['sa_no'], rec['dt'], rec['division'] ,rec['item_id'], rec['item_price'], rec['item_value']))
#                print(rec)
            elif rec['sa_flag'] == 2:
                # 出荷
                print('出荷No' + str(rec['sa_no']) + ' 商品ID:' + rec['item_id'])
                inventory_allocation(cur, rec)
            else:
                print('これは無い。イレギュラー')

            # 次レコードRead
            count = count + 1
            rec = result[count]
            next_key = rec['dt']

            # Key日付が変わったら在庫推移に登録
            if current_key != next_key:

                #在庫推移テーブル登録
                sql_ins = 'insert into t_stock_transition(stock_dt, arrival_no, arrival_dt, arrival_division, item_id, arrival_price, stock_value) select %s, arrival_no, arrival_dt, arrival_division, item_id, arrival_price, stock_value from w_stock order by arrival_no, arrival_dt, item_id'
                res = cur.execute(sql_ins, current_key)

                current_key = next_key

        except IndexError:
            print("IndexErrorをキャッチ")
            #ここで最終の在庫推移テーブル登録処理が必要
            #在庫推移テーブル登録
            sql_ins = 'insert into t_stock_transition(stock_dt, arrival_no, arrival_dt, arrival_division, item_id, arrival_price, stock_value) select %s, arrival_no, arrival_dt, arrival_division, item_id, arrival_price, stock_value from w_stock order by arrival_no, arrival_dt, item_id'
            res = cur.execute(sql_ins, current_key)

            cur.close
            loop_end = 1

            connection.commit()
finally:
    connection.close()
